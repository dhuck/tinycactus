# dhuck's Foundation for Jekyll

This repository is a quick project start combining Foundation 6.5 with Jekyll 3.83. To get started, clone this repository and run the following command to preview your site:

```sh
bundle exec jekyll serve
```

That's it! You're now building a site. Jekyll handles all of the sass building so there is no need to for extra steps like other repositories that have been posted,
